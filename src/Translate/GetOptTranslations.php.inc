<?php

return [
	'usage-title'             => __('Usage: '),
	'usage-command'           => __('command'),
	'usage-options'           => __('options'),
	'usage-operands'          => __('operands'),
	'operands-title'          => __('Operands:') . PHP_EOL,
	'options-title'           => __('Options:') . PHP_EOL,
	'commands-title'          => __('Commands:') . PHP_EOL,
	'option-unknown'          => __('Option \'%s\' is unknown'),
	'no-more-operands'        => __('No more operands expected - got %s'),
	'operand-missing'         => __('Operand %s is required'),
	'option-argument-missing' => __('Option \'%s\' must have a value'),
	'value-invalid'           => __('%s has an invalid value'),
];
