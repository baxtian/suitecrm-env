<?php

declare(strict_types=1);

/*
	Copyright (c) 2005 Steven Armstrong <sa at c-area dot ch>
	Copyright (c) 2009 Danilo Segan <danilo@kvota.net>
	Copyright (c) 2016 Michal Čihař <michal@cihar.com>

	This file is part of MoTranslator.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

use SuitecrmEnv\Translate\MoTranslator;

// @codeCoverageIgnoreStart

/*
 * Translates a string, alias for _gettext.
 *
 * @param string $msgid String to be translated
 * @return string translated string (or original, if not found)
 */
if (!function_exists('__')) {
	function __(string $msgid): string
	{
		return MoTranslator::get_instance()->gettext($msgid);
	}
}

/*
 * Translates a string with context, alias for _pgettext.
 *
 * @param string $msgid
 * @param string $context
 * @return string 
 */
if (!function_exists('_x')) {
	function _x(string $msgid, string $context): string
	{
		return MoTranslator::get_instance()->pgettext($msgid, $context);
	}
}

// @codeCoverageIgnoreEnd
