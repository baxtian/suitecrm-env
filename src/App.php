<?php

namespace SuitecrmEnv;

use SuitecrmEnv\Translate\MoTranslator;
use SuitecrmEnv\Manager\Manifest;
use SuitecrmEnv\Manager\Files;
use Locale;
use GetOpt\Option;
use GetOpt\GetOpt;
use Dotenv\Dotenv;
use Codedungeon\PHPCliColors\Color;
use DIRECTORIO_API;
use Exception;

/**
 * SuitecrmEnv\App
 * @codeCoverageIgnore
 */
class App
{
	public const VERSION = '0.8.4';

	private static function i18n()
	{
		// i18n
		$i18n = MoTranslator::get_instance();
		$i18n->setLocale(Locale::getDefault(), __DIR__ . '/../languages/');
	}

	public static function cli()
	{
		if (php_sapi_name() != 'cli') {
			die('Must run from command line');
		}

		// Inicializar i18n
		self::i18n();

		// Cli
		$getOpt = new GetOpt(
			null,
			[GetOpt::SETTING_STRICT_OPERANDS => true]
		);
		GetOpt::setLang(__DIR__ . DIRECTORY_SEPARATOR . 'Translate' . DIRECTORY_SEPARATOR . 'GetOptTranslations.php.inc');
		$getOpt->addOptions([
			Option::create('i', 'increment', GetOpt::NO_ARGUMENT)
				->setDescription(__('Increment the version number')),
			Option::create(null, 'harvest', GetOpt::OPTIONAL_ARGUMENT)
				->setDescription(__('Harvest from SuiteCRM directory. ' .
				'If no argument submited this script will look for variable ' .
				'SUITECRM_PATH in enviroment files.')),
			Option::create('y', null, GetOpt::NO_ARGUMENT)
				->setDescription(__("When harvesting, answer 'yes' to all questions.")),
			Option::create('n', null, GetOpt::NO_ARGUMENT)
				->setDescription(__("When harvesting, answer 'no' to all questions.")),
			Option::create('d', 'dist', GetOpt::NO_ARGUMENT)
				->setDescription(__('Create installer')),
			Option::create('h', 'help', GetOpt::NO_ARGUMENT)
				->setDescription(__('Show this help screen')),
			Option::create('V', 'version', GetOpt::NO_ARGUMENT)
				->setDescription(__('Version')),
		]);

		$message = false;
		$warning = false;

		try {
			// Obtener datos de la línea de comandos
			$getOpt->process();
			$options = $getOpt->getOptions();

			if (isset($options['version'])) {
				echo 'suitecrm-env' . PHP_EOL;
				echo __('Manage a SuiteCRM module dev enviroment.') . PHP_EOL;
				echo sprintf(__('Version %s'), self::VERSION) . PHP_EOL;
			} elseif (isset($options['help'])) {
				echo $getOpt->getHelpText();
			} else {

				// Administrador de archivos
				$files = new Files();
				if (isset($options['y'])) {
					$files->set_copy_option(Files::COPY_ALL_NEW);
				}
				if (isset($options['n'])) {
					$files->set_copy_option(Files::COPY_NONE_NEW);
				}

				// Administrador de manifiesto
				$manifest = new Manifest(['files' => $files]);

				// Si se incluye la opción apra incrementar la versión
				if (isset($options['increment'])) {
					$manifest->current_date();

					// Si pasó la opción "-i" incrementa el mayor, si pasó "-ii" incrementa el menor,
					// si pasó "-iii" (o cualquier otro caso) incrementa el patch 
					// incrementa el mayor. Internamente detecta si hay composer y en caso de que exista
					// usa el valor de versión en composer en cambio

					$increment_level = Manifest::INCREMENT_VERSION_PATCH_NUMBER;

					switch($options['increment']) {
						case 1:
							$increment_level = Manifest::INCREMENT_VERSION_MAJOR_NUMBER;
							break;
						case 2:
							$increment_level = Manifest::INCREMENT_VERSION_MINOR_NUMBER;
							break;
						default:
							$increment_level = Manifest::INCREMENT_VERSION_PATCH_NUMBER;
					}	

					$version = $manifest->version($increment_level);
					$message = sprintf(__("Version changed to '%s'."), $version);
				}

				// Si se incluye la opción apra cosechar
				if (isset($options['harvest'])) {
					$harvest = $options['harvest'];
					// Si es numérico y es 1, entonces usar la variable de entorno
					if (is_numeric($harvest) && $harvest == 1) {
						$dotenv = Dotenv::createImmutable(getcwd());
						$dotenv->safeLoad();
						if (!isset($_ENV['SUITECRM_PATH'])) {
							throw new Exception(__('No enviroment file found with SUITECRM_PATH field.'));
						}

						$harvest = $_ENV['SUITECRM_PATH'];
					}

					$manifest->current_date();
					$path    = $manifest->harvest($harvest);
					$message = sprintf(__("Harvested changes from '%s'."), $path);
				}

				// Si se incluye la opción apra crear el archivo de distribución
				if (isset($options['dist'])) {
					$manifest->current_date();
					$filename = $manifest->dist();
					$message  = sprintf(__("The installation file '%s' has been created."), $filename);
				}
			}

			// Si hubo mensaje imprimirlo
			if ($message) {
				echo  $message . ' ' . Color::GREEN . '✔' . Color::RESET . PHP_EOL;
			}

			// Si hubo advertencia imprimirla
			if ($warning) {
				echo Color::RED . __('Warning:') . Color::RESET . ' ' . $warning . PHP_EOL;
			}
		} catch (Exception $e) {
			$message = $e->getMessage();

			echo Color::BG_RED . __('Error:') . Color::RESET . ' ' . $message . PHP_EOL;
		}
	}

	public static function increment()
	{
		// Inicilaizar i18n
		self::i18n();

		// Administrador de manifiesto
		$manifest = new Manifest();

		// Asignar fecha actual
		$manifest->current_date();

		// Asignar versión
		$version = $manifest->version(Manifest::SYNC_WITH_COMPOSER_VERSION);

		// Mensaje
		$message = sprintf(__("Version changed to '%s'."), $version);
		echo  $message . ' ' . Color::GREEN . '✔' . Color::RESET . PHP_EOL;
	}

	public static function harvest($options = [])
	{
		// Inicilaizar i18n
		self::i18n();

		// Datos de entorno
		$dotenv = Dotenv::createImmutable(getcwd());
		$dotenv->safeLoad();
		if (!isset($_ENV['SUITECRM_PATH'])) {
			throw new Exception(__('No enviroment file found with SUITECRM_PATH field.'));
		}

		$harvest = $_ENV['SUITECRM_PATH'];

		// Administrador de manifiesto
		$manifest = new Manifest();

		// Fecha actual
		$manifest->current_date();

		// Cosechar y obtener dirección
		$path = $manifest->harvest($harvest);

		// Mensaje
		$message = sprintf(__("Harvested changes from '%s'."), $path);
		echo  $message . ' ' . Color::GREEN . '✔' . Color::RESET . PHP_EOL;
	}
}
