<?php

namespace SuitecrmEnv\Lib;

use cli\Streams;

/**
 * SuitecrmEnv\Cli
 */
class Cli
{
	/**
	 * Displays an input prompt. If no default value is provided the prompt will
	 * continue displaying until input is received.
	 *
	 * @param string      $question The question to ask the user.
	 * @param bool|string $default  A default value if the user provides no input.
	 * @param boolean     $hide     Optionally hides what the user types in.
	 * @return string     The users input.
	 */
	public static function prompt($question, $default = false, $hide = false)
	{
		// Mostrar el mensaje con : al final
		$question = (empty($default)) ? 
			sprintf(_x('%s: ', 'Prompt format'), $question) :
			sprintf(_x('%s [%s]: ', 'Prompt format with default value'), $question, $default);

		while (true) {
			Streams::out($question);
			$line = Streams::input(null, $hide);

			if (trim($line) !== '') {
				return $line;
			}
			if ($default !== false) {
				return $default;
			}
		}
	}

	/**
	 * Presents a user with a multiple choice question, useful for 'yes/no' type
	 * questions (which this public static function defaults too).
	 *
	 * @param string      $question  The question to ask the user.
	 * @param null|string $choice    A string of characters allowed as a response. Case is ignored.
	 * @param null|string $default   The default choice. NULL if a default is not allowed.
	 * @return string     The users choice.
	 */
	public static function choose($question, $choice = null, $default = null)
	{
		if(is_null($choice)) {
			$choice =  _x('yn', 'Yes or no');
		}

		if(is_null($default)) {
			$default =  _x('n', 'No');
		}

		if (!is_string($choice)) {
			$choice = join('', $choice);
		}

		// Make every choice character lowercase except the default
		$choice = str_ireplace($default, strtoupper($default), strtolower($choice));
		// Seperate each choice with a forward-slash
		$choices = trim(join('/', preg_split('//', $choice)), '/');

		while (true) {
			$line = Streams::prompt(sprintf(_x('%s? [%s] ', 'Choose format'), $question, $choices), $default, '');

			if (!empty($line) && stripos($choice, $line) !== false) {
				return strtolower($line);
			}
			if (!empty($default)) {
				return strtolower($default);
			}
		}
	}
}
