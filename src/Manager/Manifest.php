<?php

namespace SuitecrmEnv\Manager;

use SuitecrmEnv\Manager\Files;
use Baxtian\SingletonTrait;
use Exception;

/**
 * SuitecrmEnv\Manager\Files
 * @property-read Files $files
 */
class Manifest
{
	use SingletonTrait;

	public const INCREMENT_VERSION_MAJOR_NUMBER = 100;
	public const INCREMENT_VERSION_MINOR_NUMBER = 101;
	public const INCREMENT_VERSION_PATCH_NUMBER = 102;
	public const SYNC_WITH_COMPOSER_VERSION     = 103;

	public const ERROR_MANIFEST_WITHOUT_VERSION_FIELD                  = 1000;
	public const ERROR_MANIFEST_WITHOUT_MODULE_NAME_FIELD              = 1001;
	public const ERROR_COMPOSER_WITHOUT_VERSION_FIELD                  = 1002;
	public const ERROR_MANIFEST_VERSION_FIELD_FORMAT_UNEXPECTED        = 1003;
	public const ERROR_MANIFEST_WITHOUT_PUBLISHED_DATE_FIELD           = 1004;
	public const ERROR_MANIFEST_PUBLISHED_DATE_FIELD_FORMAT_UNEXPECTED = 1005;
	public const ERROR_COPYING                                         = 1006;
	public const ERROR_INCREMENT_LEVEL_NOT_ALLOWED                     = 1007;

	private $dir_suitecrm;

	private $dir_install;

	private $module_name;

	// Create class constructor with a variable that will
	// contain -if any- the list of arguments
	public function __construct($arguments = [])
	{
		// Array of attributes linked to the class
		$classes = [
			'files' => Files::class,
		];

		$this->set_dependencies($arguments, $classes);

		$this->dir_suitecrm = false;
		$this->dir_install  = false;
		$this->module_name  = false;
	}

	/**
	 * Return private data
	 *
	 * @param string $name
	 * @return mixed;
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'files':
				return $this->dependency($name);

				break;
		}

		return null;
	}

	/**
	 * Actualizar datos de versión manifest.php
	 *
	 * @param int $level
	 * @return string
	 */
	public function version($level = self::SYNC_WITH_COMPOSER_VERSION)
	{
		// Debe venir un nivel de los requeridos
		if (!in_array($level, [
			self::INCREMENT_VERSION_MAJOR_NUMBER,
			self::INCREMENT_VERSION_MINOR_NUMBER,
			self::INCREMENT_VERSION_PATCH_NUMBER,
			self::SYNC_WITH_COMPOSER_VERSION,
		])) {
			throw new Exception(__('Increment level not allowed.'), self::ERROR_INCREMENT_LEVEL_NOT_ALLOWED);
		}

		$manifest_file = getcwd() . DIRECTORY_SEPARATOR . 'manifest.php';

		// Datos del manifiesto
		list($manifest, $installdefs) = $this->files->require_manifest($manifest_file);

		if (!isset($manifest['version'])) {
			throw new Exception(__("Manifest doesn't have a version field."), self::ERROR_MANIFEST_WITHOUT_VERSION_FIELD);
		}

		// Si hay composer.json, usar la versión definida en el archivo
		$composer_file = getcwd() . DIRECTORY_SEPARATOR . 'composer.json';
		$composer      = $this->files->get_composer($composer_file);

		if (!preg_match('/^[0-9\.]*$/', $manifest['version'])) {
			throw new Exception(__("The 'version' field in the manifest is not in the expected format."), self::ERROR_MANIFEST_VERSION_FIELD_FORMAT_UNEXPECTED);
		}

		// $old_version = $manifest['version'];
		$version     = explode('.', $manifest['version']);

		if ($level == self::INCREMENT_VERSION_PATCH_NUMBER) {
			$version[2] += 1;
		} elseif ($level == self::INCREMENT_VERSION_MINOR_NUMBER) {
			$version[1] += 1;
		} else {
			$version[0] += 1;
		}
		$version = implode('.', $version);

		// Si hay datos del composer
		if ($composer) {
			if (!isset($composer->version)) {
				throw new Exception(__("Composer file doesn't have version field."), self::ERROR_COMPOSER_WITHOUT_VERSION_FIELD);
			}

			$version = $composer->version;
		}

		// Modificar fecha en manifest
		$old_version   = $this->files->search_data($manifest_file, "'version'[\s]*=> [']?([0-9\s.]*)[']?");
		$version_label = $this->files->search_data($manifest_file, "('version'[\s]*=> )[']?[0-9\s.]*[']?");
		if (empty($old_version) && empty($version_label)) {
			// @codeCoverageIgnoreStart
			throw new Exception(__("The 'version' field in the manifest is not in the expected format."), self::ERROR_MANIFEST_VERSION_FIELD_FORMAT_UNEXPECTED);
			// @codeCoverageIgnoreEnd
		}

		// Modificar versión en manifest
		$this->files->replace_content($manifest_file, "{$version_label}'{$old_version}'", "{$version_label}'{$version}'");
		$this->files->replace_content($manifest_file, "{$version_label}{$old_version}", "{$version_label}'{$version}'");

		return $version;
	}

	/**
	 * Actualizar la fecha de generación en manifest.php
	 *
	 * @return string
	 */
	public function current_date()
	{
		$manifest_file = getcwd() . DIRECTORY_SEPARATOR . 'manifest.php';

		// Datos del manifiesto
		list($manifest, $installdefs) = $this->files->require_manifest($manifest_file);

		if (!isset($manifest['published_date'])) {
			throw new Exception(__("Manifest doesn't have a 'published_date' field."), self::ERROR_MANIFEST_WITHOUT_PUBLISHED_DATE_FIELD);
		}

		// Modificar fecha en manifest
		$search_date       = $this->files->search_data($manifest_file, "'published_date'[\s]*=> '([0-9\s:-]*)'");
		$search_date_label = $this->files->search_data($manifest_file, "('published_date'[\s]*=> )'[0-9\s:-]*'");
		if (empty($search_date) && empty($search_date_label)) {
			throw new Exception(__("The 'published_date' field in the manifest is not in the expected format."), self::ERROR_MANIFEST_PUBLISHED_DATE_FIELD_FORMAT_UNEXPECTED);
		}

		$now = date('Y-m-d G:i:s');
		$this->files->replace_content($manifest_file, "{$search_date_label}'{$search_date}'", "{$search_date_label}'{$now}'");

		return $now;
	}

	/**
	 * Crear el zip de instalación
	 *
	 * @return string
	 */
	public function dist()
	{
		$dir = getcwd();

		return $this->files->create_zip($dir);
	}

	/**
	 * Copiar un item tipo definición del manifiesto
	 *
	 * @param string $from
	 * @param string $module
	 * @param string $dir
	 * @return void
	 */
	private function copy_def($from, $module, $dir)
	{
		$file_crm = implode(DIRECTORY_SEPARATOR, [
			$this->dir_suitecrm,
			'custom',
			'Extension',
			'modules',
			$module,
			'Ext',
			$dir,
			basename($from),
		]);
		$file_local = str_replace('<basepath>', $this->dir_install, $from);

		$this->files->copy($file_crm, $file_local);
	}

	/**
	 * Copiar un item tipo copy del manifiesto
	 *
	 * @param string $from
	 * @param string $to
	 * @return void
	 */
	private function copy_copy($from, $to)
	{
		$file_crm   = $this->dir_suitecrm . DIRECTORY_SEPARATOR . $to;
		$file_local = str_replace('<basepath>', $this->dir_install, $from);

		$this->files->copy($file_crm, $file_local);
	}

	/**
	 * Copiar un item tipo metadata del manifiesto
	 *
	 * @param string $meta_data
	 * @return void
	 */
	private function copy_metadata($meta_data)
	{
		$file_crm = implode(
			DIRECTORY_SEPARATOR,
			[
				$this->dir_suitecrm,
				'custom',
				'metadata',
				basename($meta_data),
			]
		);
		$file_local = str_replace('<basepath>', $this->dir_install, $meta_data);

		$this->files->copy($file_crm, $file_local);
	}

	/**
	 * Copiar un item tipo language del manifiesto
	 *
	 * @param string $from
	 * @param string $module
	 * @param string $language
	 * @return void
	 */
	private function copy_language($from, $module, $language)
	{
		$file_crm = implode(
			DIRECTORY_SEPARATOR,
			[
				$this->dir_suitecrm,
				'custom',
				'Extension',
				($module == 'application') ? $module : 'modules' . DIRECTORY_SEPARATOR . $module,
				'Ext',
				'Language',
				$language . '.' . $this->module_name . '.php',
			]
		);
		$file_local = str_replace('<basepath>', $this->dir_install, $from);

		$this->files->copy($file_crm, $file_local);
	}

	/**
	 * Copiar los datos del CRM en sus correspondientes directorios del instalador
	 *
	 * @param string $suitecrm_path
	 * @return string
	 */
	public function harvest($suitecrm_path)
	{
		// Datos del manifiesto
		$manifest_file                = getcwd() . DIRECTORY_SEPARATOR . 'manifest.php';
		list($manifest, $installdefs) = $this->files->require_manifest($manifest_file);

		// Directorio de SuiteCRM
		$suitecrm_path = realpath($suitecrm_path);
		$this->files->check_suitecrm_path($suitecrm_path);

		// ¿Fata el nombre del módulo?
		if (!isset($manifest['name'])) {
			throw new Exception(__("Manifest doesn't have a 'name' field."), self::ERROR_MANIFEST_WITHOUT_MODULE_NAME_FIELD);
		}

		// Directorios y nombre del módulo
		$this->dir_suitecrm = $suitecrm_path;
		$this->dir_install  = getcwd();
		$this->module_name  = str_replace(' ', '', $installdefs['id']);

		// Copy
		if (isset($installdefs['copy'])) {
			foreach ($installdefs['copy'] as $item) {
				// $file_crm   = $dir_crm . DIRECTORY_SEPARATOR . $item['to'];
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from']) || !isset($item['to'])) {
					throw new Exception(sprintf(__('Error copying: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_copy($item['from'], $item['to']);
			}
		}

		// Layoutdefs
		if (isset($installdefs['layoutdefs'])) {
			foreach ($installdefs['layoutdefs'] as $item) {
				// $file_crm   = implode(DIRECTORY_SEPARATOR, [$dir_crm, 'custom', 'Extension', 'modules', $item['to_module'], 'Ext', 'Layoutdefs', basename($item['from'])]);
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from']) || !isset($item['to_module'])) {
					throw new Exception(sprintf(__('Error copying layoutdef: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_def($item['from'], $item['to_module'], 'Layoutdefs');
			}
		}

		// Vardefs
		if (isset($installdefs['vardefs'])) {
			foreach ($installdefs['vardefs'] as $item) {
				// $file_crm   = implode(DIRECTORY_SEPARATOR, [$dir_crm, 'custom', 'Extension', 'modules', $item['to_module'], 'Ext', 'Vardefs', basename($item['from'])]);
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from']) || !isset($item['to_module'])) {
					throw new Exception(sprintf(__('Error copying vardef: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_def($item['from'], $item['to_module'], 'Vardefs');
			}
		}

		// Administration
		if (isset($installdefs['administration'])) {
			foreach ($installdefs['administration'] as $item) {
				// $file_crm   = implode(DIRECTORY_SEPARATOR, [$dir_crm, 'custom', 'Extension', 'modules', 'Administration', 'Ext', 'Administration', basename($item['from'])]);
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from'])) {
					throw new Exception(sprintf(__('Error copying administration: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_def($item['from'], 'Administration', 'Administration');
			}
		}

		// Scheduledefs
		if (isset($installdefs['scheduledefs'])) {
			foreach ($installdefs['scheduledefs'] as $item) {
				// $file_crm   = implode(DIRECTORY_SEPARATOR, [$dir_crm, 'custom', 'Extension', 'modules', 'Schedulers', 'Ext', 'ScheduledTasks', basename($item['from'])]);
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from'])) {
					throw new Exception(sprintf(__('Error copying schedule: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_def($item['from'], 'Schedulers', 'ScheduledTasks');
			}
		}

		// Relationships
		if (isset($installdefs['relationships'])) {
			foreach ($installdefs['relationships'] as $item) {
				// $file_crm = implode(
				// 	DIRECTORY_SEPARATOR,
				// 	[
				// 		$dir_crm,
				// 		'custom',
				// 		'metadata',
				// 		basename($item['meta_data']),
				// 	]
				// );
				// $file_local = str_replace('<basepath>', $dir_install, $item['meta_data']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['meta_data'])) {
					throw new Exception(sprintf(__('Error copying relationship: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_metadata($item['meta_data']);
			}
		}

		// Languajes
		if (isset($installdefs['language'])) {
			foreach ($installdefs['language'] as $item) {
				// $file_crm = implode(
				// 	DIRECTORY_SEPARATOR,
				// 	[
				// 		$dir_crm,
				// 		'custom',
				// 		'Extension',
				// 		'modules',
				// 		$item['to_module'],
				// 		'Ext',
				// 		'Language',
				// 		$item['language'] . '.' . $module_name . '.php',
				// 	]
				// );
				// $file_local = str_replace('<basepath>', $dir_install, $item['from']);

				// $this->files->copy($file_crm, $file_local);

				if (!isset($item['from']) || !isset($item['to_module']) || !isset($item['language'])) {
					throw new Exception(sprintf(__('Error copying language: %s'), join(', ', $item)), self::ERROR_COPYING);
				}
				$this->copy_language($item['from'], $item['to_module'], $item['language']);
			}
		}

		return $suitecrm_path;
	}
}
