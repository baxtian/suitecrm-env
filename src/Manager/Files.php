<?php

namespace SuitecrmEnv\Manager;

use SuitecrmEnv\Lib\Exec;
use SuitecrmEnv\Lib\Cli;
use Baxtian\SingletonTrait;
use Exception;

/**
 * SuitecrmEnv\Manager\Files
 * @property-read Exec $exec
 * @property-read Cli $cli
 */
class Files
{
	use SingletonTrait;
	public const ASK_COPY_NEW  = 0;
	public const COPY_ALL_NEW  = 1;
	public const COPY_NONE_NEW = 2;

	public const ERROR_COPY_OPTION_NOT_ALLOWED           = 1000;
	public const ERROR_FILE_DOES_NOT_EXISTS              = 1001;
	public const ERROR_MANIFEST_FILE_DOES_NOT_EXISTS     = 1002;
	public const ERROR_MANIFEST_FILE_NAME_DOES_NOT_MATCH = 1003;
	public const ERROR_MANIFEST_WITHOUT_EXPECTED_FIELDS  = 1004;
	public const ERROR_NOT_COMPOSER                      = 1005;
	public const ERROR_NOT_SUITECRM_PATH                 = 1006;
	public const ERROR_CANNOT_CREATE_DIRECTORY           = 1007;
	public const ERROR_CANNOT_CREATE_SUBDIRECTORY        = 1008;
	public const ERROR_ERROR_REFERENCE_DIRECTORY         = 1009;

	private $copy_option;

	/**
	 * Constructor de la clase.
	 */
	public function __construct($arguments = [])
	{
		// Array of attributes linked to the class
		$classes = [
			'exec' => Exec::class,
			'cli'  => Cli::class,
		];

		$this->set_dependencies($arguments, $classes);

		$this->copy_option = self::ASK_COPY_NEW;
	}

	/**
	 * Return private data
	 *
	 * @param string $name
	 * @return mixed;
	 */
	public function __get($name)
	{
		switch ($name) {
			case 'exec':
			case 'cli':
				return $this->dependency($name);

				break;
		}

		return null;
	}

	/**
	 * Asignar la opción al copiar archivos que no existen en el entorno de desarrollo.
	 * ASK_COPY_NEW: preguntar si dese o no copiar el archivo
	 * COPY_ALL_NEW: copiar todos los archivos
	 * COPY_NONE_NEW: no copiar los archivos nuevos
	 *
	 * @param integer $copy_option Tipo de comportamiento.
	 * @return void
	 */
	public function set_copy_option($copy_option)
	{
		if (!in_array(
			$copy_option,
			[
				self::ASK_COPY_NEW,
				self::COPY_ALL_NEW,
				self::COPY_NONE_NEW,
			]
		)) {
			throw new Exception(__('Value not allowed as copy option.'), self::ERROR_COPY_OPTION_NOT_ALLOWED);
		}

		$this->copy_option = $copy_option;
	}

	/**
	 * Buscar elemento en archivo y retornar listado de resultados
	 *
	 * @param string $filename
	 * @param string $search
	 * @return string
	 */
	public function search_data($file, $search)
	{
		// Leer el archivo style.css
		$file_data = $this->get_content($file);
		// Buscar el elemento
		if (preg_match_all('/' . $search . '/', $file_data, $result)) {
			// Si hubo respuesta
			if (isset($result[1][0])) {
				return $result[1][0];
			}
		}
	}

	/**
	 * Copiar un directorio en otro directorio
	 *
	 * @param string $sourceDirectory
	 * @param string $destinationDirectory
	 * @param string $childFolder
	 * @return void
	 * @codeCoverageIgnore
	 */
	private function copy_dir($sourceDirectory, $destinationDirectory, $childFolder = '')
	{
		if (is_dir($destinationDirectory) === false) {
			try {
				mkdir($destinationDirectory);
			} catch (Exception $e) {
				if(empty($destinationDirectory)) {
					throw new Exception(sprintf(__("Can't create directory using as reference: %s"), $sourceDirectory), self::ERROR_ERROR_REFERENCE_DIRECTORY);	
				}
				throw new Exception(sprintf(__("Can't create directory: %s"), $destinationDirectory), self::ERROR_CANNOT_CREATE_DIRECTORY);
			}
		}

		$directory = opendir($sourceDirectory);

		if ($childFolder !== '') {
			if (is_dir("$destinationDirectory/$childFolder") === false) {
				try {
					mkdir("$destinationDirectory/$childFolder");
				} catch (Exception $e) {
					throw new Exception(sprintf(__("Can't create directory: %s/%s"), $destinationDirectory, $childFolder), self::ERROR_CANNOT_CREATE_SUBDIRECTORY);
				}
			}

			while (($file = readdir($directory)) !== false) {
				if ($file === '.' || $file === '..') {
					continue;
				}

				if (is_dir("$sourceDirectory/$file") === true) {
					$this->copy_dir("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file", $childFolder);
				} else {
					$this->copy_file("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
				}
			}

			closedir($directory);

			return;
		}

		while (($file = readdir($directory)) !== false) {
			if ($file === '.' || $file === '..') {
				continue;
			}

			if (is_dir("$sourceDirectory/$file") === true) {
				$this->copy_dir("$sourceDirectory/$file", "$destinationDirectory/$file", $childFolder);
			} else {
				$this->copy_file("$sourceDirectory/$file", "$destinationDirectory/$file");
			}
		}

		closedir($directory);
	}

	/**
	 * Copiar archivos
	 *
	 * @param string $origin
	 * @param string $destination
	 * @return void
	 */
	private function copy_file($origin, $destination)
	{
		// Si el archivo  de destino no existe, indagar primero
		if (!file_exists($destination)) {
			// Si la opciónd e copia es no copiar los nuevos, saltar.
			if ($this->copy_option == self::COPY_NONE_NEW) {
				return;
			}

			// Si la opción de copia es preguntar
			if ($this->copy_option == self::ASK_COPY_NEW) {
				$filename = str_replace(getcwd(), '<basepath>', $destination);
				$question = sprintf(__("Do you want to copy the file '%s'"), $filename);

				$copy = $this->cli->choose($question);

				// Si la opción seleccionada es 'no' saltar
				if ($copy == 'n') {
					return;
				}
			}
		}

		copy($origin, $destination);
	}

	/**
	 * Copia un archivo o un directorio e un lugar a otro
	 *
	 * @param string $from
	 * @param string $to
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function copy($from, $to)
	{
		if (is_dir($from)) {
			$this->copy_dir(realpath($from), realpath($to), '');
		} elseif (file_exists($from)) {
			$this->copy_file($from, $to);
		}
	}

	/**
	 * Repace content in file
	 *
	 * @param string $filename Nombre del archivo
	 * @param string $find Texto a buscar
	 * @param string $replace Texto por el que será reemplazado
	 * @return void
	 */
	public function replace_content($filename, $find, $replace)
	{
		file_put_contents($filename, str_replace($find, $replace, $this->get_content($filename)));
	}

	/**
	 * Leer el contenido de un archivo
	 *
	 * @param string $filename
	 * @return string
	 */
	public function get_content($filename)
	{
		if (!file_exists($filename)) {
			throw new Exception(sprintf(__("File doesn't exists: %s"), basename($filename)), self::ERROR_FILE_DOES_NOT_EXISTS);
		}

		return file_get_contents($filename);
	}

	/**
	 * Obtener datos del manifiesto
	 *
	 * @param string $filename Archivo de manifiesto
	 * @return array manifiesto ($manifest) y definición de instalación ($installdefs).
	 */
	public function require_manifest($filename)
	{
		if (basename($filename) != 'manifest.php') {
			throw new Exception(__('Manifest file name does not match.'), self::ERROR_MANIFEST_FILE_NAME_DOES_NOT_MATCH);
		}

		if (!file_exists($filename)) {
			throw new Exception(__('Manifest file not available.'), self::ERROR_MANIFEST_FILE_DOES_NOT_EXISTS);
		}

		require($filename);

		if (!isset($manifest) || !isset($installdefs) || !isset($installdefs['id'])) {
			throw new Exception(__("Manifest file doesn't have required fields."), self::ERROR_MANIFEST_WITHOUT_EXPECTED_FIELDS);
		}

		return [$manifest, $installdefs];
	}

	/**
	 * Obtener datos del composer
	 *
	 * @param string $filename Archivo de compoer
	 * @return false|object Datos del composer.
	 */
	public function get_composer($filename)
	{
		if (!file_exists($filename)) {
			return false;
		}

		$content = $this->get_content($filename);

		return json_decode($content);
	}

	/**
	 * Crear el archivo de distribución a partir del compando git
	 *
	 * @param strin] $zipname
	 * @return void
	 */
	public function create_zip($directory)
	{
		if (!file_exists($directory . DIRECTORY_SEPARATOR . 'composer.json')) {
			throw new Exception(__('This is not a composer directory.'), self::ERROR_NOT_COMPOSER);
		}

		// Nombre del archivo
		$filename = basename(getcwd());
		$zipname  = "{$filename}.zip";

		// Comprimir
		$this->exec->exec("composer archive --format=zip --file={$filename}");

		return $zipname;
	}

	/**
	 * Verifica que el directorio sí es un SuiteCRM
	 *
	 * @param string $path
	 * @return boolean
	 */
	public function check_suitecrm_path($path)
	{
		if (!file_exists($path . DIRECTORY_SEPARATOR . 'sugar_version.php')) {
			throw new Exception(sprintf(__("The path to be harvested doesn't look like a SuiteCRM directory: %s"), $path), self::ERROR_NOT_SUITECRM_PATH);
		}

		return true;
	}
}
