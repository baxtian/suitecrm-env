<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use SuitecrmEnv\Lib\Cli;

 class LibCliTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			 ->returnArg(1);
 		Monkey\Functions\when('_x')
			 ->returnArg(1);
 	}

 	public function testPromptWithoutDefaultArgument(): void
 	{
 		// Una primera vez no habrá respuesta y por lo tanto se harán
 		// dos llamados a out y a input.
 		// En la última retornará 'input' que será el valor esperado
 		$cli = Mockery::mock('alias:cli\Streams');
 		$cli->shouldReceive('out')
			->with('Question: ')
			->times(2);
 		$cli->shouldReceive('input')
			->times(2)
			->andReturn('', 'answer');

 		$data = Cli::prompt('Question');

 		// Verificación
 		$this->assertEquals('answer', $data);
 	}

 	public function testPromptWithDefaultArgument(): void
 	{
 		// Una primera vez no habrá respuesta y por lo tanto se harán
 		// dos llamados a out y a input.
 		// En la última retornará 'input' que será el valor esperado
 		$cli = Mockery::mock('alias:cli\Streams');
 		$cli->shouldReceive('out')
			->with('Question [default]: ');
 		$cli->shouldReceive('input')
			->andReturn('');

 		$data = Cli::prompt('Question', 'default');

 		// Verificación
 		$this->assertEquals('default', $data);
 	}

 	public function testChoose(): void
 	{
 		// Una primera vez se usará como respuesta 'y', la segunda 'n'
 		// y la tercera se usará el valor por defecto.
 		// Se espera que imprima en el formato de consulta
 		$cli = Mockery::mock('alias:cli\Streams');
 		$cli->shouldReceive('prompt')
			->withArgs(['Question? [y/N] ', 'n', ''])
			->andReturn('y', 'n', '');

 		// Verificaciones
 		$data = Cli::choose('Question');
 		$this->assertEquals('y', $data);

 		$data = Cli::choose('Question');
 		$this->assertEquals('n', $data);

 		$data = Cli::choose('Question');
 		$this->assertEquals('n', $data);
 	}

 	public function testChooseWithCustomOptions(): void
 	{
 		// Se espera que imprima en el formato de consulta pata a y b
 		$cli = Mockery::mock('alias:cli\Streams');
 		$cli->shouldReceive('prompt')
			->withArgs(['Question? [a/B] ', 'b', ''])
			->andReturn('a', 'b', '');

 		// Verificaciones
 		$data = Cli::choose('Question', ['a', 'b'], 'b');
 		$this->assertEquals('a', $data);

 		$data = Cli::choose('Question', 'ab', 'b');
 		$this->assertEquals('b', $data);

 		$data = Cli::choose('Question', 'ab', 'b');
 		$this->assertEquals('b', $data);
 	}

	 public function testChooseWithCustomOptionsInArray(): void
 	{
 		// Se espera que imprima en el formato de consulta pata a y b
 		$cli = Mockery::mock('alias:cli\Streams');
 		$cli->shouldReceive('prompt')
			->withArgs(['Question? [c/D] ', 'd', ''])
			->andReturn('c', 'd', '');

 		// Verificaciones
 		$data = Cli::choose('Question', ['c', 'd'], 'd');
 		$this->assertEquals('c', $data);

 		$data = Cli::choose('Question', ['c', 'd'], 'd');
 		$this->assertEquals('d', $data);

 		$data = Cli::choose('Question', ['c', 'd'], 'd');
 		$this->assertEquals('d', $data);
 	}
 }
