<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;
 use Exception;

 // Clases y dependencias a probar
 use SuitecrmEnv\Manager\Files;
 use SuitecrmEnv\Lib\Exec;

 class ManagerFilesTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			 ->returnArg(1);
 		Monkey\Functions\when('_x')
			 ->returnArg(1);
 	}

 	public function testInit(): void
 	{
 		// Verificación
 		$files = new Files();

 		$copy_option = $this->getProperty($files, 'copy_option');
 		$this->assertEquals(files::ASK_COPY_NEW, $copy_option);
 	}

 	public function testMagicRead()
 	{
 		// Verificación
 		$files = new Files();

 		$this->assertTrue(is_null($files->no_arg));
 	}

 	public function testSetCopyOption(): void
 	{
 		// Verificación
 		$files = new Files();

 		// Cambio de opción de copia
 		$files->set_copy_option(files::COPY_NONE_NEW);
 		$copy_option = $this->getProperty($files, 'copy_option');
 		$this->assertEquals(files::COPY_NONE_NEW, $copy_option);
 	}

 	public function testDispatchErrorCopyOptionNotAllowed(): void
 	{
 		$this->expectExceptionCode(files::ERROR_COPY_OPTION_NOT_ALLOWED);

 		// Verificación
 		$files = new Files();

 		// Intentar una opción de copia fuea de lo permitidp
 		$files->set_copy_option(10);
 	}

 	public function testSearchData(): void
 	{
 		// Verificación
 		$files    = new Files();
 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'search_data.txt';
 		$text1    = $files->search_data($filename, 'Text to find ([0-9]*)');
 		$text2    = $files->search_data($filename, 'No find ([0-9]*)');

 		// Cambio de opción de copia
 		$this->assertEquals('1234', $text1);
 		$this->assertEquals('', $text2);
 	}

 	public function testGetContent(): void
 	{
 		// Verificación
 		$files    = new Files();
 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'simple_text.txt';
 		$text     = $files->get_content($filename);

 		// Cambio de opción de copia
 		$this->assertEquals('Lorem ipsum' . PHP_EOL, $text);
 	}

 	public function testDispatchErrorFileDoesNotExists(): void
 	{
 		$this->expectExceptionCode(files::ERROR_FILE_DOES_NOT_EXISTS);
 		$this->expectExceptionMessage("File doesn't exists: inexistent_file.txt");

 		// Verificación
 		$files    = new Files();
 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'inexistent_file.txt';
 		$text     = $files->get_content($filename);
 	}

 	public function testReplaceContent(): void
 	{
 		$tmp = tempnam('/tmp', 'test_foo');
 		file_put_contents($tmp, '123456');

 		// Verificación
 		$files = new Files();
 		$files->replace_content($tmp, '456', '321');

 		// Cambio de opción de copia
 		$text = $files->get_content($tmp);
 		$this->assertEquals('123321', $text);
 	}

 	public function testRequireManifest(): void
 	{
 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'manifest.php';

 		// Verificación
 		$files                        = new Files();
 		list($manifest, $installdefs) = $files->require_manifest($filename);

 		$this->assertEquals(1, $manifest['a']);
 		$this->assertEquals(2, $manifest['b']);
 		$this->assertEquals(3, $installdefs['c']);
 		$this->assertEquals(4, $installdefs['d']);
 	}

 	public function testDispatchErrorManifestNameDoesNotMatch(): void
 	{
 		$this->expectExceptionCode(files::ERROR_MANIFEST_FILE_NAME_DOES_NOT_MATCH);

 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'simple_text.txt';

 		// Verificación
 		$files                        = new Files();
 		list($manifest, $installdefs) = $files->require_manifest($filename);
 	}

 	public function testDispatchErrorManifestFileDoesNotExists(): void
 	{
 		$this->expectExceptionCode(files::ERROR_MANIFEST_FILE_DOES_NOT_EXISTS);

 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'inexistsent_dir' . DIRECTORY_SEPARATOR . 'manifest.php';

 		// Verificación
 		$files                        = new Files();
 		list($manifest, $installdefs) = $files->require_manifest($filename);
 	}

 	public function testDispatchErrorIncompleteManifest(): void
 	{
 		$this->expectExceptionCode(files::ERROR_MANIFEST_WITHOUT_EXPECTED_FIELDS);

 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'incomplete' . DIRECTORY_SEPARATOR . 'manifest.php';

 		// Verificación
 		$files                        = new Files();
 		list($manifest, $installdefs) = $files->require_manifest($filename);
 	}

 	public function testDispatchErrorNoIdInManifest(): void
 	{
 		$this->expectExceptionCode(files::ERROR_MANIFEST_WITHOUT_EXPECTED_FIELDS);

 		$filename = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'no_id' . DIRECTORY_SEPARATOR . 'manifest.php';

 		// Verificación
 		$files                        = new Files();
 		list($manifest, $installdefs) = $files->require_manifest($filename);
 	}

 	public function testGetComposer(): void
 	{
 		$filename  = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'composer.json';
 		$files     = new Files();
 		$composer1 = $files->get_composer($filename);
 		$composer2 = $files->get_composer($filename . 'no');

 		// Verificación
 		$this->assertEquals('1', $composer1->a);
 		$this->assertFalse($composer2);
 	}

 	public function testCreateZip(): void
 	{
 		$exec = Mockery::mock(Exec::class);
 		$exec->shouldReceive('exec')
			->andReturn(true);

 		$dir = dirname(__DIR__, 1);

 		// Verificación
 		$files    = new Files(['exec' => $exec]);
 		$zip_file = $files->create_zip($dir);

 		$this->assertEquals('suitecrm-env.zip', $zip_file);
 	}

 	public function testDispatchErrorNoComposerDirectory()
 	{
 		$this->expectExceptionCode(files::ERROR_NOT_COMPOSER);

 		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'incomplete';

 		// Verificación
 		$files    = new Files();
 		$zip_file = $files->create_zip($dir);
 	}

 	public function testCheckSuitecrmPath(): void
 	{
 		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'suitecrm';

 		// Verificación
 		$files = new Files();
 		$check = $files->check_suitecrm_path($dir);

 		$this->assertTrue($check);
 	}

 	public function testDispatchErrorNoSuitecrmPath()
 	{
 		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'incomplete';
 		$this->expectExceptionCode(files::ERROR_NOT_SUITECRM_PATH);
 		$this->expectExceptionMessage("The path to be harvested doesn't look like a SuiteCRM directory: {$dir}");

 		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'incomplete';

 		// Verificación
 		$files = new Files();
 		$files->check_suitecrm_path($dir);
 	}

 	public function testCopyFile()
 	{
 		$destination = tempnam('/tmp', 'test_foo');
 		$origin      = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'simple_text.txt';

 		// Verificación
 		$files = new Files();
 		$this->callMethod($files, 'copy_file', [$origin, $destination]);
 		$this->assertFileEquals($origin, $destination);
 	}

 	public function testCopyFileInexistentDestination()
 	{
 		$origin      = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'simple_text.txt';
 		$destination = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'inexistent_file.txt';

 		$cli      = Mockery::mock(Cli::class);
 		$question = sprintf("Do you want to copy the file '%s'", str_replace(getcwd(), '<basepath>', $destination));
 		$cli->shouldReceive('choose')
			->withArgs([$question])
			->andReturn('n', 'y');
 		$files = new Files(['cli' => $cli]);

 		// Verificación con COPY_NONE_NEW
 		$files->set_copy_option(Files::COPY_NONE_NEW);
 		$this->callMethod($files, 'copy_file', [$origin, $destination]);
 		$this->assertFalse(file_exists($destination));

 		// Verificación con ASK_COPY_NEW y respuesta N
 		$files->set_copy_option(Files::ASK_COPY_NEW);
 		$this->callMethod($files, 'copy_file', [$origin, $destination]);
 		$this->assertFalse(file_exists($destination));

 		// Verificación con ASK_COPY_NEW y respuesta Y
 		$files->set_copy_option(Files::ASK_COPY_NEW);
 		$this->callMethod($files, 'copy_file', [$origin, $destination]);
 		$this->assertTrue(file_exists($destination));
 		unlink($destination);
 	}
 }
