<?php

 namespace Tests;

 use Mockery\Adapter\Phpunit\MockeryTestCase;
 use Brain\Monkey;
 use ReflectionClass;

 class MonkeyTestCase extends MockeryTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\setUp();
 	}
	
 	protected function tearDown(): void
 	{
 		Monkey\tearDown();
 		parent::tearDown();
 	}

	/**
	 * Llamar métodos privados o protejidos
	 *
	 * @param object $object
	 * @param string $methodName
	 * @param array $parameters
	 * @return void
	 */
 	protected function callMethod(&$object, $methodName, array $parameters = [])
 	{
 		$reflection = new ReflectionClass(get_class($object));
 		$method     = $reflection->getMethod($methodName);
 		$method->setAccessible(true);

 		return $method->invokeArgs($object, $parameters);
 	}

	/**
	 * Asignar valor a una propiedad privada o protegida
	 *
	 * @param object $object
	 * @param string $propertyName
	 * @param mixed $value
	 * @return void
	 */
 	protected function setProperty(&$object, $propertyName, $value)
 	{
 		$reflection = new ReflectionClass(get_class($object));
 		$property   = $reflection->getProperty($propertyName);
 		$property->setAccessible(true);
 		$property->setValue($object, $value);
 	}

	/**
	 * Obtener el valor de una propiedad privada o protegida
	 *
	 * @param object $object
	 * @param string $propertyName
	 * @return mixed
	 */
	protected function getProperty(&$object, $propertyName)
 	{
 		$reflection = new ReflectionClass(get_class($object));
 		$property   = $reflection->getProperty($propertyName);
 		$property->setAccessible(true);
 		return $property->getValue($object);
 	}
 }
