<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use SuitecrmEnv\Manager\Manifest;
 use SuitecrmEnv\Manager\Files;

 class ManagerManifestTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			 ->returnArg(1);
 		Monkey\Functions\when('_x')
			 ->returnArg(1);
 	}

 	public function testMagicRead()
 	{
 		// Verificación
 		$manifest = new Manifest();

 		$this->assertTrue(is_null($manifest->no_arg));
 	}

 	public function testIncrementVersionLevelMajor(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => '1.1',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(false);
 		$files->shouldReceive('get_content')
			->andReturn('');
 		$files->shouldReceive('replace_content')
			->andReturn('');
 		$files->shouldReceive('search_data')
			->andReturn('version');

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version(Manifest::INCREMENT_VERSION_MAJOR_NUMBER);
 		$this->assertEquals('2.1', $new_version);
 	}

	 public function testIncrementVersionLevelMinor(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => '1.1',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(false);
 		$files->shouldReceive('get_content')
			->andReturn('');
 		$files->shouldReceive('replace_content')
			->andReturn('');
 		$files->shouldReceive('search_data')
			->andReturn('version');

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version(Manifest::INCREMENT_VERSION_MINOR_NUMBER);
 		$this->assertEquals('1.2', $new_version);
 	}

	 public function testIncrementVersionPatch(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => '1.1.1',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(false);
 		$files->shouldReceive('get_content')
			->andReturn('');
 		$files->shouldReceive('replace_content')
			->andReturn('');
 		$files->shouldReceive('search_data')
			->andReturn('version');

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version(Manifest::INCREMENT_VERSION_PATCH_NUMBER);
 		$this->assertEquals('1.1.2', $new_version);
 	}

 	public function testIncrementVersionLevelComposer(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => '1.1.1',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(json_decode('{"version": "2.2.2"}'));
 		$files->shouldReceive('replace_content')
			->andReturn('');
 		$files->shouldReceive('search_data')
			->andReturn('version');

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version(Manifest::SYNC_WITH_COMPOSER_VERSION);
 		$this->assertEquals('2.2.2', $new_version);
 	}

 	public function testDispatchErrorIncrementLevelNotAllowed(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_INCREMENT_LEVEL_NOT_ALLOWED);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[],
					[],
				]
			);

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version(1000);
 	}

 	public function testDispatchErrorManifestWithoutVersionField(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_MANIFEST_WITHOUT_VERSION_FIELD);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[],
					[],
				]
			);

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version();
 	}

 	public function testDispatchErrorComposerWithoutVersionField(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COMPOSER_WITHOUT_VERSION_FIELD);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => '1.1',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(json_decode('{"name": "name"}'));

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version();
 	}

 	public function testDispatchErrorManifestVersionFieldFormatUnexpected(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_MANIFEST_VERSION_FIELD_FORMAT_UNEXPECTED);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'version' => 'abc',
					],
					[],
				]
			);
 		$files->shouldReceive('get_composer')
			->andReturn(json_decode('{"version": "0.1"}'));

 		// Verificación
 		$manifest    = new Manifest(['files' => $files]);
 		$new_version = $manifest->version();
 	}

 	public function testUpdateDate(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[
						'published_date' => 'date',
					],
					[],
				]
			);
 		$files->shouldReceive('search_data')
			->andReturn('date');
 		$files->shouldReceive('replace_content')
			->andReturn('date');

 		// Verificación
 		$now      = date('Y-m-d G:i:s');
 		$manifest = new Manifest(['files' => $files]);
 		$date     = $manifest->current_date();
 		$this->assertEquals($now, $date);
 	}

 	public function testDispatchErrorManifestWithoutPublishedDate(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_MANIFEST_WITHOUT_PUBLISHED_DATE_FIELD);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn(
				[
					[],
					[],
				]
			);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$date     = $manifest->current_date();
 	}

 	public function testDispatchErrorManifestPublishedDateFormatUnexpected(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_MANIFEST_PUBLISHED_DATE_FIELD_FORMAT_UNEXPECTED);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'published_date' => 'date',
				],
				[],
			]);
 		$files->shouldReceive('search_data')
			->andReturn('');

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$date     = $manifest->current_date();
 	}

 	public function testDist(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('create_zip')
			->andReturn('file.zip');

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$filename = $manifest->dist();
 		$this->assertEquals('file.zip', $filename);
 	}

 	public function testHarvest(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'       => 'module',
					'language' => [
						[
							'from'      => 'a',
							'to_module' => 'b',
							'language'  => 'en_EN',
						],
					],
					'relationships' => [
						[
							'meta_data' => 'a',
						],
					],
					'scheduledefs' => [
						[
							'from' => 'a',
						],
					],
					'administration' => [
						[
							'from' => 'a',
						],
					],
					'vardefs' => [
						[
							'from'      => 'a',
							'to_module' => 'b',
						],
					],
					'layoutdefs' => [
						[
							'from'      => 'a',
							'to_module' => 'b',
						],
					],
					'copy' => [
						[
							'from' => 'a',
							'to'   => 'b',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);
 		$files->shouldReceive('copy')
			->times(7)
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$path     = $manifest->harvest('/root');
 		$this->assertEquals('/root', $path);
 	}

 	public function testDispatchErrorManifestWithoutName(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_MANIFEST_WITHOUT_MODULE_NAME_FIELD);

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'published_date' => 'date',
				],
				[],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingCopy(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'   => 'module',
					'copy' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingLayoutdef(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying layoutdef: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'         => 'module',
					'layoutdefs' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingVardef(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying vardef: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'      => 'module',
					'vardefs' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingAdministration(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying administration: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'             => 'module',
					'administration' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingScheduledef(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying schedule: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'           => 'module',
					'scheduledefs' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingReltionship(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying relationship: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'            => 'module',
					'relationships' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testDispatchErrorCopyingLanguage(): void
 	{
 		$this->expectExceptionCode(Manifest::ERROR_COPYING);
 		$this->expectExceptionMessage('Error copying language: a');

 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('require_manifest')
			->andReturn([
				[
					'name'           => 'module',
					'published_date' => 'date',
				],
				[
					'id'       => 'module',
					'language' => [
						[
							'force_error' => 'a',
						],
					],
				],
			]);
 		$files->shouldReceive('check_suitecrm_path')
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$manifest->harvest('/root');
 	}

 	public function testCopyCopy(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('copy')
			->withArgs(['dir_suitecrm' . DIRECTORY_SEPARATOR . 'b', 'dir_install' . DIRECTORY_SEPARATOR . 'a'])
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$this->setProperty($manifest, 'dir_suitecrm', 'dir_suitecrm');
 		$this->setProperty($manifest, 'dir_install', 'dir_install');
 		$this->callMethod($manifest, 'copy_copy', ['<basepath>/a', 'b']);
 		$this->assertTrue(true);
 	}

 	public function testCopyDef(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('copy')
		 ->withArgs([join(DIRECTORY_SEPARATOR, [
		 	'dir_suitecrm',
		 	'custom',
		 	'Extension',
		 	'modules',
		 	'b',
		 	'Ext',
		 	'c',
		 	'a',
		 ]), 'dir_install' . DIRECTORY_SEPARATOR . 'a'])
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$this->setProperty($manifest, 'dir_suitecrm', 'dir_suitecrm');
 		$this->setProperty($manifest, 'dir_install', 'dir_install');
 		$this->callMethod($manifest, 'copy_def', ['<basepath>/a', 'b', 'c']);
 		$this->assertTrue(true);
 	}

 	public function testCopyMetadata(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('copy')
		 ->withArgs([join(DIRECTORY_SEPARATOR, [
		 	'dir_suitecrm',
		 	'custom',
		 	'metadata',
		 	'a',
		 ]), 'dir_install' . DIRECTORY_SEPARATOR . 'a'])
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$this->setProperty($manifest, 'dir_suitecrm', 'dir_suitecrm');
 		$this->setProperty($manifest, 'dir_install', 'dir_install');
 		$this->callMethod($manifest, 'copy_metadata', ['<basepath>/a']);
 		$this->assertTrue(true);
 	}

 	public function testCopyLanguage(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('copy')
		 ->withArgs([join(DIRECTORY_SEPARATOR, [
		 	'dir_suitecrm',
		 	'custom',
		 	'Extension',
		 	'modules',
		 	'b',
		 	'Ext',
		 	'Language',
		 	'c.const.php',
		 ]), 'dir_install' . DIRECTORY_SEPARATOR . 'a'])
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$this->setProperty($manifest, 'module_name', 'const');
 		$this->setProperty($manifest, 'dir_suitecrm', 'dir_suitecrm');
 		$this->setProperty($manifest, 'dir_install', 'dir_install');
 		$this->callMethod($manifest, 'copy_language', ['<basepath>/a', 'b', 'c']);
 		$this->assertTrue(true);
 	}

 	public function testCopyLanguageApplication(): void
 	{
 		$files = Mockery::mock(Files::class);
 		$files->shouldReceive('copy')
		 ->withArgs([join(DIRECTORY_SEPARATOR, [
		 	'dir_suitecrm',
		 	'custom',
		 	'Extension',
		 	'application',
		 	'Ext',
		 	'Language',
		 	'c.const.php',
		 ]), 'dir_install' . DIRECTORY_SEPARATOR . 'a'])
			->andReturn(true);

 		// Verificación
 		$manifest = new Manifest(['files' => $files]);
 		$this->setProperty($manifest, 'module_name', 'const');
 		$this->setProperty($manifest, 'dir_suitecrm', 'dir_suitecrm');
 		$this->setProperty($manifest, 'dir_install', 'dir_install');
 		$this->callMethod($manifest, 'copy_language', ['<basepath>/a', 'application', 'c']);
 		$this->assertTrue(true);
 	}
 }
