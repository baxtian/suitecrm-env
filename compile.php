<?php

require_once __DIR__ . '/vendor/autoload.php';

use Codedungeon\PHPCliColors\Color;

function compile($pharFile, $stubFile)
{
	$buildRoot = __DIR__;

	// clean up
	if (!is_dir('dist')) {
		mkdir('dist');
	}
	if (file_exists($pharFile)) {
		unlink($pharFile);
	}
	if (file_exists($pharFile . '.gz')) {
		unlink($pharFile . '.gz');
	}

	// create phar
	$p = new Phar($pharFile);

	// Include vendor
	$exclude = '/^(?!(.*dist\/xgettext-timber|.*tests\/|.*tmp|.*\.vscode|.*\.git))(.*)$/i';

	// creating our library using whole directory
	$p->buildFromDirectory($buildRoot, $exclude);

	// pointing main file which requires all classes
	$p->setStub("#!/usr/bin/env php\n\n" . $p->createDefaultStub($stubFile));

	chmod($pharFile, 0755);

	$message = "$pharFile successfully created";
	echo Color::GREEN . $message . ' ✔' . Color::RESET . PHP_EOL;
}

compile('dist/suitecrm-env.phar', 'suitecrm-env.php');
