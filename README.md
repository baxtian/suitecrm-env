# SuiteCRM-Env

**Harvest files from a SuiteCRM directory into a development module an create an installable module.**

This library add a composer enviroment that reads a manifest.php file and harvest all these files from a SuiteCRM directory. The main idea is allow to develop directly in the SuiteCRM directory and copy all the changes at the end of the process.

## Prerequisites

You need a **CRM** directory and a **module** directory. The **module** has to have the *manifest.php*. 

Also your system requires to have [composer](https://getcomposer.org/) installed. 

## Install the library

In case your **module** doesn't have a *composer.json* create it with these data

	{
		"name": "workspace/module_name",
		"version": "1.1.1"
	}

Now run the next command

	composer install --dev baxtian/suitecrm-dev

Add to the *composer.json* file the *scripts* and *archive* elements.

	{
		.
		.
		.
		"scripts": {
			"version": "SuitecrmEnv\\App::increment",
			"harvest": "SuitecrmEnv\\App::harvest",
			"dist": "composer archive --format=zip",
			"release": [
				"@version",
				"@dist"
			]
		},
		"archive": {
			"exclude": [
				"/.vscode/",
				"/.env",
				"/.env.example",
				"/vendor/",
				".gitignore",
				"composer.*",
				"README.md",
				"*.zip"
			]
		}
		.
		.
		.
	}

And finally add or update the *.env* file to have the path to  your *CRM* directory.

	SUITECRM_PATH=/path/to/your/crm

## How to use this?

* Do your changes to the files directly in the CRM.
* If you created a module and modified it with Studio remember to copy the files from *custom/Extension/modules/your_module/Ext/metadata* to *modules/your_module/metadata*
* If you added a new file inside a module, your manifest will copy the entire directory and will ask if you want to copy the new file.
* If you added a new file outside a module you have to add the definition inside *manifest.php* to allow **suitecrm-env** to copy the new file or files. Read the documentation about the [manifest](https://docs.suitecrm.com/developer/module-installer/).

When the changes are ready, run this command

	composer harvest

This will copy any changes in the CRM to your module.

To create an installation file run
	
	composer dist

Also to change the version and the timestamp update the *version* field in *composer.json* and run the command

	composer release

